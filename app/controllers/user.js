const crypto = require("crypto");

const DATA_CRIPTO = {
  algoritmo: "aes256",
  secret: "criptografar a senha rs",
  type: "hex"
};

const User = require("../models/user");

async function create(ctx) {
  const newUsuario = new User(ctx.request.body);

  const cipher = crypto.createCipher(DATA_CRIPTO.algoritmo, DATA_CRIPTO.secret);
  cipher.update(newUsuario.password);

  newUsuario.password = cipher.final(DATA_CRIPTO.type);

  await newUsuario.save();

  ctx.body = newUsuario;
}

async function login(ctx) {
  const email = ctx.request.body.email;
  const password = ctx.request.body.password;

  const user = await User.findOne({ email });

  const cipher = crypto.createCipher(DATA_CRIPTO.algoritmo, DATA_CRIPTO.secret);
  cipher.update(password);

  const hashPass = cipher.final(DATA_CRIPTO.type);

  if (!user || hashPass !== user.password) {
    ctx.body = {
      msg: "Invalid User"
    };

    ctx.status = 500;

    return;
  }

  ctx.body = {
    name: user.name,
    email: user.email
  };
}

module.exports = {
  create,
  login
};
