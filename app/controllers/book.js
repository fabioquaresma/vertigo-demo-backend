const Book = require("../models/book");

async function findAll(ctx) {
  const books = await Book.find({});
  ctx.body = books;
}

async function findByCategory(ctx) {
  const category = ctx.params.category;
  const books = await Book.find({ category });
  ctx.body = books;
}

async function create(ctx) {
  const newBook = new Book(ctx.request.body);
  const savedBook = await newBook.save();
  ctx.body = savedBook;
}

module.exports = {
  findAll,
  findByCategory,
  create
};
