module.exports = router => {
  router.prefix("/api/v1");
  router.use("/books", require("./book"));
  router.use("/user", require("./user"));
};
