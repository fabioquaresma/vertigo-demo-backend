const Router = require("koa-router");
const router = new Router();

const controller = require("../controllers/book");

router.get("/", controller.findAll);
router.post("/", controller.create);
router.get("/:category", controller.findByCategory);

module.exports = router.routes();
