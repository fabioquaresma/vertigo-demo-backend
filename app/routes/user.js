const Router = require("koa-router");
const router = new Router();
const controller = require("../controllers/user");

router.post("/", controller.create);
router.post("/login", controller.login);

module.exports = router.routes();
