const mongoose = require("mongoose");

const BookSchema = new mongoose.Schema(
  {
    tittle: { type: String },
    author: { type: String },
    category: { type: String }
  },
  { timestamps: true }
);

mongoose.model("Book", BookSchema);

module.exports = mongoose.model("Book");
